<?php // $Id$ ?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?>">
    <div class="cB">
  <div class="cBw"><div class="cBt"><div></div></div>
<?php if ($block->subject): ?>
  <p class="block-title"><?php print $block->subject ?></p>
<?php endif;?>

  <div class="content">
    <?php print $block->content ?>
  </div>
  
    <div class="cBb"><div></div></div></div>
</div>
  
</div>