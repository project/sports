<?php // $Id$ ?>
<?php

function sports_preprocess_page(&$vars, $hook) {

    $classes = split(' ', $vars['body_classes']);
    
    if(empty($vars['footer_right'])){
        $classes[] = t('no-footer-right');
    } else{
        $classes[] = t('has-footer-right');
    }
    
    $vars['body_classes_array'] = $classes;
    $vars['body_classes'] = implode(' ', $classes);
    
}


?>