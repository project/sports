<?php // $Id$  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>

<?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
  <!--[if IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie7.css";</style>
<![endif]-->
<!--[if IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6.css";</style>
<![endif]-->
</head>

<body class="<?php print $body_classes; ?>">

<div id="wrapper">
<div id="wrapper-inner">
    <div id="header">
	
	  <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
   
         <?php if (!empty($secondary_links)): ?>
          <div id="secondary" class="clear-block">
         
            <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
	     </div>
       
	   <?php endif; ?>
    
     
        
      <div id="logo-title">
      
       <?php if (!empty($logo)): ?>
       <div id="logo">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
          </div>
        <?php endif; ?>

        <div id="site-name">
       
           <?php if (!empty($site_name)): ?>
       <div id="site-name-title">
            <h1> <?php print $site_name; ?>
            </h1> </div>
          <?php endif; ?>
          
 
        </div> <!-- /site name -->
      </div> <!-- /logo-title -->
    </div> <!-- /header -->
	
	 <div id="slogan-search">
	 
	   <?php if (!empty($search_box)): ?>
        <div id="search-box"><?php print $search_box; ?></div>
      <?php endif; ?>

	   <?php if (!empty($site_slogan)): ?>
	   
            <div id="site-slogan">
         
            
            <?php print $site_slogan; ?>
        
            </div>
          <?php endif; ?>

	  </div> <!-- /slogan and search  -->
	  
	   <div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; } ?> ">
        <?php if (!empty($primary_links)): ?>
        
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
		
        <?php endif; ?>

      </div> <!-- /navigation -->
	 
    <div id="container">
    
         <?php if (!empty($mission)): ?>
         <div id="mission">
         
         <?php print $mission; ?>
         </div>
         <?php endif; ?>
	
       <?php if (!empty($left)): ?>
        <div id="sidebar-left" class="column sidebar">
          <?php print $left; ?>
          <img src="/sites/all/themes/sports/images/column-one.jpg" width="214" height="300" alt="" class="background-image"/>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>
		
      <div id="main">
	  <div id="main-squeeze">
     


        <div id="content">
		
   <div class="cB">
  <div class="cBw"><div class="cBt"><div></div></div>
		
		
                 <?php if (!empty($title)): ?><h2 class="title" id="page-title"><?php print $title; ?></h2><?php endif; ?>
          <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
  
        
          
          <?php if (!empty($help)): print $help; endif; ?>
          <div id="content-content" class="clear-block">
            <?php print $content; ?>
            
           

          </div> <!-- /content-content -->
         
		   <div class="cBb"><div></div></div></div>
</div>


   <?php if (!empty($content_bottom)): ?>
    <div id="content-bottom">
 
       
          <?php print $content_bottom; ?>
   
  </div> <!-- /cotent bottem -->
      <?php endif; ?>
		 
        </div> <!-- /content -->
      </div></div> <!-- /main-squeeze /main -->

       
     <?php if (!empty($right)): ?>
        <div id="sidebar-right" class="column sidebar">
          <?php print $right; ?>
          <img src="/sites/all/themes/sports/images/column-two.jpg" width="214" height="300" alt="" class="background-image"/>
        </div> <!-- /sidebar-right -->
      <?php endif; ?>

    </div> <!-- /container -->
    


    <div id="footer-wrapper">
	
	 
	  
      <div id="footer">
       
		   <?php if (!empty($footer_left)): ?>
        <div id="footer-one">
          <?php print $footer_left; ?>
        </div> <!-- /footer-one -->
      <?php endif; ?>
	 
		  <?php if (!empty($footer_right)): ?>
        <div id="footer-two">
          <?php print $footer_right; ?>
        </div> <!-- /footer-two -->
      <?php endif; ?>
		
      </div> <!-- /footer -->
      
      
      <div id="footer-message">
	 
	 
	     <?php if (!empty($footer_message)): ?>
        <div id="footer-text">
          <?php print $footer_message; ?>
        </div> <!-- /footer-text -->
      <?php endif; ?>
    
</div>
      
      
    </div> <!-- /footer-wrapper -->
    
	</div> <!-- /wrapper inner -->
  </div> <!-- /wrapper -->
  
  <div id="designer">
 <p><a href="http://www.suburban-glory.com/">London Web Design</a></p>
 </div>
  
  

  <?php print $closure; ?>
</body>
</html>